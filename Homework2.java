
/**
@author Zachary Blaise
@category recursion homework
@version 1.0
**/

package Hw2test;


public class Homework2{
	/**
	Problem 1:
	Comparing two strings with Compareto recursive method.
	**/
	public static int compareToIgnorecase(String s1, String s2){
		if(s1.length() == 0){
			return 0;
		}
		else if(s1.charAt(0) < s2.charAt(0)){
			return -1;
		}
		else if(s1.charAt(0) > s2.charAt(0)){
			return 1;
		}
		else {
			return compareToIgnorecase(s1.substring(1), s2.substring(1));
		}
	}
}