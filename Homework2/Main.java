
import java.util.Scanner;

/**
 * @author Zachary Blaise
 */
public class Main {

  public static void main(String[] args) {
    Alphabetizer alpha = new Alphabetizer();
    Scanner input = new Scanner(System.in);

    while(true) {
      System.out.println("Input command index:\n1) Compare alphabetically\n2) Find first alphabetically\n3) Exit");
      int i = input.nextInt();

      System.out.println("Input first string:");
      String s1 = input.nextLine();
      System.out.println("Input second string:");
      String s2 = input.nextLine();

      if (i == 1) {
        int result = alpha.compareTo(s1, s2);
        if (result == -1)
          System.out.println(s1 + " comes before " + s2);
        else if (result == 0)
          System.out.println(s1 + " is the same as " + s2);
        else if (result == 1)
          System.out.println(s1 + " comes after " + s2);
      } else if (i == 2)
        System.out.println();
      else if (i == 3)
        System.exit(0);
      else
        System.out.println("Invaid command index");

      System.out.println("\n==========\n");
    }
  }
}
