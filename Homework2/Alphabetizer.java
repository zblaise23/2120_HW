import java.util.ArrayList;

public class Alphabetizer {

  public int compareTo(String s1, String s2) {
    char c1 = s1.toLowerCase().charAt(0);
    char c2 = s2.toLowerCase().charAt(0);

    if (c1 < c2 || (s1.length() == 1 && s2.length() > 1))
      return -1;
    else if (c1 > c2 || (s2.length() == 1 && s1.length() > 1))
      return 1;

    if (s1.length() == 1 && s2.length() == 1)
      return 0;

    return compareTo(s1.substring(1), s2.substring(1));
  }

  public String findMinimum(ArrayList<String> words) {
    if (words.size() < 2)
      return words.get(0);

    int result = compareTo(words.get(0), words.get(1));
    if (result == 0 || result == -1)
      words.remove(1);
    else if (result == 1)
      words.remove(0);

    return findMinimum(words);
  }
}
