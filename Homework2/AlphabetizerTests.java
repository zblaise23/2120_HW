import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.Ignore;
import java.util.ArrayList;

public class AlphabetizerTests {

  private Alphabetizer alpha;
  private String word1 = "alphaCat";
  private String word2 = "alphaDog";
  private String word3 = "alphaTiger";

	@Before
	public void setUp() {
    alpha = new Alphabetizer();
  }

	@Test
	public void compareTo_s1ComesBeforeS2_ReturnsIntegerLessThan0() {
    int actual = alpha.compareTo(word1, word2);
    int expected = -1;

    assertEquals(expected, actual);
	}

	@Test
	public void compareTo_s1EqualsS2_Returns0() {
    int actual = alpha.compareTo(word1, word1);
    int expected = 0;

    assertEquals(expected, actual);
	}

	@Test
	public void compareTo_s1ComesAfterS2_ReturnsIntegerGreaterThan0() {
    int actual = alpha.compareTo(word2, word1);
    int expected = 1;

    assertEquals(expected, actual);
	}

  @Test
  public void findMinimum_s1ComesFirst_ReturnsCorrectString() {
    ArrayList words = new ArrayList<String>();
    words.add(word1);
    words.add(word2);
    words.add(word3);

    String actual = alpha.findMinimum(words);
    String expected = word1;

    assertEquals(expected, actual);
  }
}
