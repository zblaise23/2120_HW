import java.awt.*;
import javax.swing.*;

public class GameOfLife {
    public static void main(String[] args) throws InterruptedException {

        int xDim = 75;
        int yDim = 100;
        GameOfLifeGUI theGUI = new GameOfLifeGUI(xDim,yDim);

        JFrame f = new JFrame("Game of Life");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setSize(theGUI.getCellSize() * xDim, theGUI.getCellSize() * yDim);
        f.add(theGUI);
        f.setVisible(true);

		while (true) {
            if (!theGUI.paused)
                theGUI.run();
            f.repaint();
        }
    }
}
