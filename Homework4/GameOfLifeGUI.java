/**
*@author Zachary Blaise
*@assignment Homework 4
*@version 1.0
**/
import java.awt.Color;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionEvent;
import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

public class GameOfLifeGUI extends JPanel implements Observer{

	private Grid myGrid;
	private int xDim;
	private int yDim;
	private int cellSize;
	public boolean paused = true;

	public GameOfLifeGUI(int xDim, int yDim) {
		super();
		this.xDim = xDim;
		this.yDim = yDim;
		myGrid = new Grid(xDim, yDim);
		myGrid.gliderSetup();
		addMouseListener(new MyMouseListener());
		this.cellSize = 10;
	}
	public void update(Observable o, Object arg) {

	}

	public int getCellSize() {
		return this.cellSize;
	}

	@Override
	public void paintComponent(Graphics g) {
		int pixels = 20;
		for (int x=0; x<xDim; x++) {
			for (int y = 0; y < yDim; y++){
				if (myGrid.cellIsAlive(x,y)) {
					g.setColor(Color.WHITE);
				}
				else {
					g.setColor(Color.BLACK);
					g.fillRect(x*(this.cellSize), y*(this.cellSize), (this.cellSize), (this.cellSize));
				}
			}
		}
	}
	private class MyMouseListener implements MouseListener {
		void saySomething(String eventDescription, MouseEvent e) {
			System.out.println(eventDescription + " detected on "
			+ e.getComponent().getClass().getName()
			+ ".\n");
		}

		public void mousePressed(MouseEvent e){
			saySomething("Mouse pressed; # of clicks: "
			+ e.getClickCount(), e);
		}

		public void mouseReleased(MouseEvent e) {
			saySomething("Mouse released; # of clicks: "
			+ e.getClickCount(), e);
		}

		public void mouseEntered(MouseEvent e) {
			saySomething("Mouse entered", e);
		}

		public void mouseExited(MouseEvent e) {
			saySomething("Mouse exited", e);
		}


		public void mouseClicked(MouseEvent e) {
			saySomething("Mouse clicked (# of clicks: "
			+ e.getClickCount() + ")", e);
			/** MouseEvent right click toggles pause on right click
			*	MouseEvent Left click activates cellIsAlive at an X and Y coordinate.
			**/
			if (SwingUtilities.isLeftMouseButton(e)) {
				myGrid.toggleCellAlive(e.getX() / 10, e.getY() / 10);
			}
			if (SwingUtilities.isRightMouseButton(e))
				paused = !paused;
		}
	}
	public void run() throws InterruptedException {
		// on purpose an endless loop
			myGrid.update();
			Thread.sleep(100);
	}
}
