public class ComplexCalculator {

  public ComplexCalculator() {

  }

  /**
   * Regarding assignment:
   * complexNum1: a, b
   * complexNum2: c, d
   */

  public ComplexNumber add(ComplexNumber complexNum1, ComplexNumber complexNum2) {
    float a = complexNum1.getA() + complexNum2.getA();
    float b = complexNum1.getB() + complexNum2.getB();
    return new ComplexNumber(a, b);
  }

  public ComplexNumber subtract(ComplexNumber complexNum1, ComplexNumber complexNum2) {
    float a = complexNum1.getA() - complexNum2.getA();
    float b = complexNum1.getB() - complexNum2.getB();
    return new ComplexNumber(a, b);
  }

  public ComplexNumber multiply(ComplexNumber complexNum1, ComplexNumber complexNum2) {
    float a = (complexNum1.getA() * complexNum2.getA()) - (complexNum1.getB() * complexNum2.getB());
    float b = (complexNum1.getB() * complexNum2.getA()) + (complexNum1.getA() * complexNum2.getB());
    return new ComplexNumber(a, b);
  }

  public ComplexNumber divide(ComplexNumber complexNum1, ComplexNumber complexNum2) {
    float a = ((complexNum1.getA() * complexNum2.getA()) + (complexNum1.getB() * complexNum2.getB()))
        / ((complexNum2.getA() * complexNum2.getA()) + (complexNum2.getB() * complexNum2.getB()));
    float b = ((complexNum1.getB() * complexNum2.getA()) - (complexNum1.getA() * complexNum2.getB()))
        / ((complexNum2.getA() * complexNum2.getA()) + (complexNum2.getB() * complexNum2.getB()));;
    return new ComplexNumber(a, b);
  }
}
