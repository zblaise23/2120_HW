import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class ComplexCalculatorTests {

  private ComplexCalculator calculator;
  private ComplexNumber complexNumA;
  private ComplexNumber complexNumB;

	@Before
	public void setUp() {
    calculator = new ComplexCalculator();
		complexNumA = new ComplexNumber(1f, 2f); // a, b
		complexNumB = new ComplexNumber(3f, 4f); // c, d
  }

	@Test
	public void testAdd() {
    ComplexNumber actual = calculator.add(complexNumA, complexNumB);
    ComplexNumber expected = new ComplexNumber(4f, 6f);
    assertEquals(expected.getA(), actual.getA(), 0f);
    assertEquals(expected.getB(), actual.getB(), 0f);
	}

	@Test
	public void testSubtract() {
    ComplexNumber actual = calculator.subtract(complexNumA, complexNumB);
    ComplexNumber expected = new ComplexNumber(-2f, -2f);
    assertEquals(expected.getA(), actual.getA(), 0f);
    assertEquals(expected.getB(), actual.getB(), 0f);
	}

	@Test
	public void testMult() {
    ComplexNumber actual = calculator.multiply(complexNumA, complexNumB);
    ComplexNumber expected = new ComplexNumber(-5f, 10f);
    assertEquals(expected.getA(), actual.getA(), 0f);
    assertEquals(expected.getB(), actual.getB(), 0f);
	}

	@Test
	public void testDiv() {
    ComplexNumber actual = calculator.divide(complexNumA, complexNumB); // 11/25, 2/25
    ComplexNumber expected = new ComplexNumber(11f / 25f, 2f / 25f);
    assertEquals(expected.getA(), actual.getA(), 0f);
    assertEquals(expected.getB(), actual.getB(), 0f);
	}
}
