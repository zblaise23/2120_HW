
import java.util.Scanner;

/**
 * @author Zachary Blaise
 */
public class Main {

  public static void main(String[] args) {
    ComplexCalculator calculator = new ComplexCalculator();
    Scanner input = new Scanner(System.in);

    while(true) {
      System.out.println("Input command index:\n1) Add\n2) Subtract\n3) Multiply\n4) Divide\n5) Exit");
      int i = input.nextInt();

      System.out.println("Input a for complex number 1:");
      float a1 = input.nextFloat();
      System.out.println("Input b for complex number 1:");
      float b1 = input.nextFloat();
      ComplexNumber complexNum1 = new ComplexNumber(a1, b1);

      System.out.println("Input a for complex number 2:");
      float a2 = input.nextFloat();
      System.out.println("Input b for complex number 2:");
      float b2 = input.nextFloat();
      ComplexNumber complexNum2 = new ComplexNumber(a2, b2);

      ComplexNumber result = null;

      if(i == 1)
        result = calculator.add(complexNum1, complexNum2);
      else if (i == 2)
        result = calculator.subtract(complexNum1, complexNum2);
      else if (i == 3)
        result = calculator.multiply(complexNum1, complexNum2);
      else if (i == 4)
        result = calculator.divide(complexNum1, complexNum2);
      else if (i == 5)
        System.exit(0);
      else
        System.out.println("Invaid command index");

      if (result.equals(null))
        System.out.println(result.toString());

      System.out.println("\n==========\n");
    }
  }
}
