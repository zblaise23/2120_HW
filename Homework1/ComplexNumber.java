public class ComplexNumber {

  private float a;
  private float b;

  public ComplexNumber(float a, float b) {
    this.a = a;
    this.b = b;
  }

  public float getA() {
    return a;
  }

  public float getB() {
    return b;
  }

  public String toString() {
    return a + " + " + b + "i";
  }
}
